<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221126075928 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, business_account VARCHAR(150) NOT NULL, event_account VARCHAR(150) DEFAULT NULL, last_event_account VARCHAR(150) DEFAULT NULL, file_number INT NOT NULL, civility VARCHAR(20) DEFAULT NULL, current_vehicle_owner VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, first_name VARCHAR(255) DEFAULT NULL, road_name VARCHAR(255) NOT NULL, additional_adress VARCHAR(255) DEFAULT NULL, postal_code INT DEFAULT NULL, city VARCHAR(150) NOT NULL, home_phone VARCHAR(20) DEFAULT NULL, cell_phone VARCHAR(20) DEFAULT NULL, business_phone VARCHAR(20) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, date_of_circulation DATETIME DEFAULT NULL, date_of_purchase DATETIME DEFAULT NULL, last_event_date DATETIME DEFAULT NULL, brand VARCHAR(150) DEFAULT NULL, model VARCHAR(150) DEFAULT NULL, version VARCHAR(255) DEFAULT NULL, registration_number VARCHAR(150) DEFAULT NULL, type_of_lead VARCHAR(100) DEFAULT NULL, mileage INT DEFAULT NULL, energy VARCHAR(100) DEFAULT NULL, nv_seller VARCHAR(255) DEFAULT NULL, vo_seller VARCHAR(255) DEFAULT NULL, billing_comment LONGTEXT DEFAULT NULL, type_vn_vo VARCHAR(5) DEFAULT NULL, file_number_vn_vo VARCHAR(50) DEFAULT NULL, event_date DATETIME NOT NULL, event_origin VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE customer');
    }
}
