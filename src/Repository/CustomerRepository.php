<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Service\ImportXlsxFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * @extends ServiceEntityRepository<Customer>
 *
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    protected $importXlsxFile;

    public function __construct(ManagerRegistry $registry, ImportXlsxFile $importXlsxFile)
    {
        $this->importXlsxFile = $importXlsxFile;
        parent::__construct($registry, Customer::class);
    }

    public function add(Customer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Customer $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * importation fichier excel dans la base de donnee
     * @param $dataExcel
     * @return array
     */
    public function importCustomerXlsx($dataExcel): array
    {
        $errorMessage = [];
        $sheetCount   = $dataExcel['loader']->getSheetCount();
        if ($sheetCount > 0) {
            for ($i = 0; $i < $sheetCount; $i++) {
                $sheetResult = $this->importXlsxFile->getArrayDataBySheet($dataExcel['loader'], $i);
                foreach ($sheetResult as $key => $data) {
                    //verification si le fichier possede au mois des données sur les 3 colonnes
                    if (!array_key_exists(0, $data) || !array_key_exists(1, $data) || !array_key_exists(2, $data) || !array_key_exists(3, $data)) {
                        $errorMessage[] = "Ce fichier n'est pas valide.";
                        return $errorMessage;
                    }

                    // verification si la ligne a un nom
                    if ($data[6]) {
                        $customer = new Customer();
                        $customer->setBusinessAccount($data[0]);
                        $customer->setEventAccount($data[1]);
                        $customer->setLastEventAccount($data[2]);
                        $customer->setFileNumber($data[3]);
                        $customer->setCivility($data[4]);
                        $customer->setCurrentVehicleOwner($data[5]);
                        $customer->setName($data[6]);
                        $customer->setFirstName($data[7]);
                        $customer->setRoadName($data[8]);
                        $customer->setAdditionalAdress($data[9]);
                        $customer->setPostalCode($data[10]);
                        $customer->setCity($data[11]);
                        $customer->setHomePhone($data[12]);
                        $customer->setCellPhone($data[13]);
                        $customer->setBusinessPhone($data[14]);
                        $customer->setEmail($data[15]);
                        if ($data[16]) {
                            $customer->setDateOfCirculation($this->convertDateFromExcel($data[16]));
                        }
                        if ($data[17]) {
                            $customer->setDateOfPurchase($this->convertDateFromExcel($data[17]));
                        }
                        if ($data[18]) {
                            $customer->setLastEventDate($this->convertDateFromExcel($data[18]));
                        }
                        $customer->setBrand($data[19]);
                        $customer->setModel($data[20]);
                        $customer->setVersion($data[21]);
                        $customer->setVin($data[22]);
                        $customer->setRegistrationNumber($data[23]);
                        $customer->setTypeOfLead($data[24]);
                        $customer->setMileage($data[25]);
                        $customer->setEnergy($data[26]);
                        $customer->setNvSeller($data[27]);
                        $customer->setVoSeller($data[28]);
                        $customer->setBillingComment($data[29]);
                        $customer->setTypeVnVo($data[30]);
                        $customer->setFileNumberVnVo($data[31]);
                        $customer->setNvSalesInterim($data[32]);
                        if ($data[33]) {
                            $customer->setEventDate($this->convertDateFromExcel($data[33]));
                        }
                        $customer->setEventOrigin($data[34]);
                        $this->getEntityManager()->persist($customer);
                        $this->getEntityManager()->flush();
                    }
                }
            }
        }

        return $errorMessage;
    }

    /**
     * conversion date excel par datetime
     * @param $dateExcel
     * @return \DateTimeImmutable|false
     */
    private function convertDateFromExcel($dateExcel)
    {
        $date = NumberFormat::toFormattedString($dateExcel, NumberFormat::FORMAT_DATE_DDMMYYYY);
        $date = \DateTimeImmutable::createFromFormat('d/m/Y', $date);
        return $date;
    }
}
