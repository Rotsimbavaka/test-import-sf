<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImportXlsxFile
{

    protected $entityManager;
    protected $translator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManagerInterface $entityManager,
                                TranslatorInterface    $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator    = $translator;
    }

    /**
     * @param $csvFile
     * @param $dir
     * @param $columns
     * @return array
     * @throws Exception
     */
    public function import($csvFile, $dir, $columns = null): array
    {
        ini_set('max_execution_time', 0);

        $originalName = $csvFile->getClientOriginalName();
        $pathPart     = pathinfo($originalName);
        $extension    = $pathPart['extension'];

        if ($extension !== 'xlsx')
            return [
                'status'  => 'error',
                'message' => $this->translator->trans('bo.import.xlsx.invalid.file')
            ];

        $filenameImage = md5(uniqid()) . '.' . $extension;

        $csvFile->move($dir, $filenameImage);

        $filename = $dir . $filenameImage;
        $reader   = new Xlsx();
        $reader->setReadDataOnly(true);
        $loader = $reader->load($filename);

        if (!$columns)
            $columns = 'A1:' . $loader->getSheet(0)->getHighestColumn() . '1';
        $headers = $loader->getSheet(0)->rangeToArray($columns);// retest

        $headers = isset($headers[0]) && is_array($headers[0]) ? $headers[0] : [];

        @unlink($filename);

        return [
            'header' => $headers,
            'loader' => $loader
        ];
    }

    /**
     * @param $loader
     * @param $sheetNum
     * @return array
     */
    public function getArrayDataBySheet($loader, $sheetNum): array
    {
        $activeSheet = $loader->getSheet($sheetNum);
        $sheet       = $activeSheet->toArray();
        $dataBody    = array_filter($sheet);

        array_shift($dataBody);

        // remove null array value
        $dataBody = array_filter($dataBody, function ($value) {
            if (!empty(array_filter($value)))
                return $value;
        });

        return $dataBody;
    }
}