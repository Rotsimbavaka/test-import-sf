<?php

namespace App\Controller;

use App\Form\ImportType;
use App\Repository\CustomerRepository;
use App\Service\ImportXlsxFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    /**
     * @Route("/", name="app_import")
     */
    public function index(Request $request, ImportXlsxFile $importXlsxFile, CustomerRepository $customerRepository): Response
    {
        $form = $this->createForm(ImportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $xlsFile = $request->files->get('import')['file'];
            if ($xlsFile) {
                $dir       = $this->getParameter('kernel.project_dir') . '/public/upload/xlsx_file/';
                $dataExcel = $importXlsxFile->import($xlsFile, $dir);
                /*import fichier */
                $messages = $customerRepository->importCustomerXlsx($dataExcel);

                foreach ($messages as $message) {
                    $this->addFlash('error', $message);
                }

                $this->addFlash('success', 'Importation effectuée avec succés');
                return $this->redirectToRoute('app_import', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->render('import/index.html.twig', [
            'importForm' => $form->createView(),
        ]);
    }
}
