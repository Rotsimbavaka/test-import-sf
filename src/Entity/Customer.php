<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\Length(max=150)
     * @Assert\NotBlank()
     */
    private $businessAccount;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Assert\Length(max=150)
     */
    private $eventAccount;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Assert\Length(max=150)
     */
    private $lastEventAccount;

    /**
     * @ORM\Column(type="integer")
     */
    private $fileNumber;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $currentVehicleOwner;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $roadName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $additionalAdress;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\Length(max=150)
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     */
    private $homePhone;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     */
    private $cellPhone;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Length(max=20)
     */
    private $businessPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOfCirculation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOfPurchase;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastEventDate;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Assert\Length(max=150)
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Assert\Length(max=150)
     */
    private $model;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\Length(max=50)
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Assert\Length(max=150)
     */
    private $registrationNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     */
    private $typeOfLead;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mileage;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100)
     */
    private $energy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $nvSeller;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $voSeller;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $billingComment;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Assert\Length(max=5)
     */
    private $typeVnVo;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\Length(max=50)
     */
    private $fileNumberVnVo;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     * @Assert\Length(max=150)
     */
    private $nvSalesInterim;

    /**
     * @ORM\Column(type="datetime")
     */
    private $eventDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $eventOrigin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBusinessAccount(): ?string
    {
        return $this->businessAccount;
    }

    public function setBusinessAccount(string $businessAccount): self
    {
        $this->businessAccount = $businessAccount;

        return $this;
    }

    public function getEventAccount(): ?string
    {
        return $this->eventAccount;
    }

    public function setEventAccount(?string $eventAccount): self
    {
        $this->eventAccount = $eventAccount;

        return $this;
    }

    public function getLastEventAccount(): ?string
    {
        return $this->lastEventAccount;
    }

    public function setLastEventAccount(?string $lastEventAccount): self
    {
        $this->lastEventAccount = $lastEventAccount;

        return $this;
    }

    public function getFileNumber(): ?int
    {
        return $this->fileNumber;
    }

    public function setFileNumber(int $fileNumber): self
    {
        $this->fileNumber = $fileNumber;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(?string $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getCurrentVehicleOwner(): ?string
    {
        return $this->currentVehicleOwner;
    }

    public function setCurrentVehicleOwner(?string $currentVehicleOwner): self
    {
        $this->currentVehicleOwner = $currentVehicleOwner;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getRoadName(): ?string
    {
        return $this->roadName;
    }

    public function setRoadName(?string $roadName): self
    {
        $this->roadName = $roadName;

        return $this;
    }

    public function getAdditionalAdress(): ?string
    {
        return $this->additionalAdress;
    }

    public function setAdditionalAdress(?string $additionalAdress): self
    {
        $this->additionalAdress = $additionalAdress;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(?int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getHomePhone(): ?string
    {
        return $this->homePhone;
    }

    public function setHomePhone(?string $homePhone): self
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    public function getCellPhone(): ?string
    {
        return $this->cellPhone;
    }

    public function setCellPhone(?string $cellPhone): self
    {
        $this->cellPhone = $cellPhone;

        return $this;
    }

    public function getBusinessPhone(): ?string
    {
        return $this->businessPhone;
    }

    public function setBusinessPhone(?string $businessPhone): self
    {
        $this->businessPhone = $businessPhone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateOfCirculation(): ?\DateTimeInterface
    {
        return $this->dateOfCirculation;
    }

    public function setDateOfCirculation(?\DateTimeInterface $dateOfCirculation): self
    {
        $this->dateOfCirculation = $dateOfCirculation;

        return $this;
    }

    public function getDateOfPurchase(): ?\DateTimeInterface
    {
        return $this->dateOfPurchase;
    }

    public function setDateOfPurchase(?\DateTimeInterface $dateOfPurchase): self
    {
        $this->dateOfPurchase = $dateOfPurchase;

        return $this;
    }

    public function getLastEventDate(): ?\DateTimeInterface
    {
        return $this->lastEventDate;
    }

    public function setLastEventDate(?\DateTimeInterface $lastEventDate): self
    {
        $this->lastEventDate = $lastEventDate;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }
    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getVin(): self
    {
        return $this->vin;
    }

    public function getRegistrationNumber(): ?string
    {
        return $this->registrationNumber;
    }

    public function setRegistrationNumber(?string $registrationNumber): self
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    public function getTypeOfLead(): ?string
    {
        return $this->typeOfLead;
    }

    public function setTypeOfLead(?string $typeOfLead): self
    {
        $this->typeOfLead = $typeOfLead;

        return $this;
    }

    public function getMileage(): ?int
    {
        return $this->mileage;
    }

    public function setMileage(?int $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getEnergy(): ?string
    {
        return $this->energy;
    }

    public function setEnergy(?string $energy): self
    {
        $this->energy = $energy;

        return $this;
    }

    public function getNvSeller(): ?string
    {
        return $this->nvSeller;
    }

    public function setNvSeller(?string $nvSeller): self
    {
        $this->nvSeller = $nvSeller;

        return $this;
    }

    public function getVoSeller(): ?string
    {
        return $this->voSeller;
    }

    public function setVoSeller(?string $voSeller): self
    {
        $this->voSeller = $voSeller;

        return $this;
    }

    public function getBillingComment(): ?string
    {
        return $this->billingComment;
    }

    public function setBillingComment(?string $billingComment): self
    {
        $this->billingComment = $billingComment;

        return $this;
    }

    public function getTypeVnVo(): ?string
    {
        return $this->typeVnVo;
    }

    public function setTypeVnVo(?string $typeVnVo): self
    {
        $this->typeVnVo = $typeVnVo;

        return $this;
    }

    public function getFileNumberVnVo(): ?string
    {
        return $this->fileNumberVnVo;
    }

    public function setFileNumberVnVo(?string $fileNumberVnVo): self
    {
        $this->fileNumberVnVo = $fileNumberVnVo;

        return $this;
    }

    public function getNvSalesInterim(): ?string
    {
        return $this->nvSalesInterim;
    }

    public function setNvSalesInterim(?string $nvSalesInterim): self
    {
        $this->nvSalesInterim = $nvSalesInterim;

        return $this;
    }


    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->eventDate;
    }

    public function setEventDate(\DateTimeInterface $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getEventOrigin(): ?string
    {
        return $this->eventOrigin;
    }

    public function setEventOrigin(?string $eventOrigin): self
    {
        $this->eventOrigin = $eventOrigin;

        return $this;
    }
}
