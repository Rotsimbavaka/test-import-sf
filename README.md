# test-import-sf
test technique, il s'agira de réaliser un import de fichier avec enregistrement des données en base de données.

## Dépendances
* Les dépendances Php sont installées avec [composer](https://getcomposer.org/)
* Les dépendances js sont installées avec [yarn](https://yarnpkg.com/)

#INSTALLATION DU PROJET
* Etape 1: composer install 
* Etape 2: composer dump-env dev (generation fichier: .env.local.php ),
  puis modifier la configuration et la nom de la bdd DATABASE_URL selon votre back-end server et
* Etape 3: php bin/console d:d:c (pour la création de la base de donnée)
* Etape 4: php bin/console d:m:m (pour la migration)
* Etape 5: yarn install
* Etape 6: yarn run dev
* Etape 7: symfony serve (pour lancer l'application)